<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="a simple blog">
    <meta name="author" content="asartz">
    <link rel="icon" href="../../favicon.ico">

    <title>Blogie - a simple blog</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/bower_vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/custom/css/blog.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Home</a>
          <?php

if ($session->get('user') === FALSE)
{
	?>
            <a class="blog-nav-item" href="?controller=users&action=login">Login</a>
          <?php }
else
{
	?>
            <a class="blog-nav-item" href="?controller=posts&action=index">Posts</a>
            <a class="blog-nav-item" href="?controller=users&action=logout">Logout</a>
          <?php
}
?>
        </nav>
      </div>
    </div>

    <div class="container">

    <?php require_once "routes.php";?>

    </div><!-- /.container -->

    <footer class="blog-footer">
      <p>Blogie - a simple blog</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/bower_vendor/jquery/dist/jquery.min.js"></script>
    <script src="../assets/bower_vendor/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
