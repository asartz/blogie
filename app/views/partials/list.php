<div class="blog-header">
  <h1 class="blog-title">The Blogie Blog</h1>
  <p class="lead blog-description">The official blogie blog.</p>
</div>

<div class="row">

  <div class="col-sm-8 blog-main">
<?php
foreach ($posts as $key => $value)
{
	?>
    <div class="blog-post">
      <h2 class="blog-post-title"><?php echo $value['title'];?></h2>
      <p class="blog-post-meta"><?php echo $value['updated_at'];?></p>

    </div><!-- /.blog-post -->
<?php
}
?>
  </div><!-- /.blog-main -->

</div><!-- /.row -->