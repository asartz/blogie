<form method="POST" action="?controller=posts&action=update" accept-charset="UTF-8" role="form">
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $post['title'];?>" maxlength="50">
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" name="body" id="body" placeholder="Body" maxlength="1500"><?php echo $post['body'];?></textarea>
  </div>
  <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $post['id'];?>">
  <input type="submit" class="btn btn-default" value="Update">
</form>