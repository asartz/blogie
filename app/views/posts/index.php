<div class="insert">
  <a class="btn btn-default" href="?controller=posts&action=create" role="button">New Post</a>
</div>
<table class="table table-hover table-condensed" id="dev-table">
  <thead>
    <tr>
      <th>#</th>
      <th>Title</th>
      <th>Last Updated</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
foreach ($posts as $key => $value)
{
	?>
      <tr>
        <td><a href="?controller=posts&action=edit&id=<?php print $value['id'];?>"><?php print $value['id']?></a></td>
        <td><?php print $value['title'];?></td>
        <td><?php print $value['updated_at'];?></td>
        <td><a class="btn btn-default" href="?controller=posts&action=delete&id=<?php print $value['id'];?>" role="button">Delete</a></td>
      </tr>
    <?php
}
?>
  </tbody>
</table>
