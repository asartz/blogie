<form method="POST" action="?controller=posts&action=store" accept-charset="UTF-8" role="form">
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Title" maxlength="50">
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" name="body" id="body" placeholder="Body" maxlength="1500"></textarea>
  </div>
  <input type="submit" class="btn btn-default" value="Create">
</form>