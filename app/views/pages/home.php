<div class="blog-header">
  <h1 class="blog-title">The Blogie Blog</h1>
  <p class="lead blog-description">The official blogie blog.</p>
</div>

<div class="row">

  <div class="col-sm-8 blog-main">
<?php
foreach ($posts as $key => $value)
{
	?>
    <div class="blog-post">
      <h2 class="blog-post-title"><a href="?controller=pages&action=post&id=<?php print $value['id'];?>"><?php print $value['title']?></a></h2>
      <p class="blog-post-body"><?php echo substr($value['body'], 0, 100);?></p>
      <p class="blog-post-meta"><?php echo $value['updated_at'];?></p>
    </div><!-- /.blog-post -->
<?php
}
?>
  </div><!-- /.blog-main -->

</div><!-- /.row -->