<div class="blog-header">
  <h1 class="blog-title">The Blogie Blog</h1>
  <p class="lead blog-description">The official blogie blog.</p>
</div>

<div class="row">

  <div class="col-sm-8 blog-main">
<?php
if (isset($post))
{
	?>
    <div class="blog-post">
      <h2 class="blog-post-title"><?php print $post['title']?></h2>
      <p class="blog-post-body"><?php echo $post['body'];?></p>
      <p class="blog-post-meta"><?php echo $post['updated_at'];?></p>
    </div><!-- /.blog-post -->
<?php
}
?>
  </div><!-- /.blog-main -->

</div><!-- /.row -->