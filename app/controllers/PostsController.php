<?php namespace Blogie\Controllers;

class PostsController {

	private $session;

	public function __construct(\Blogie\Libs\Session $session)
	{
		if ($session->get('user') === FALSE)
		{
			die('You are not authorized to proceed with this operation. Please login first.');
		}

		$this->session = $session;
	}

	public function index()
	{
		// we store all the posts in a variable
		$posts = \Blogie\Models\Post::all();
		require_once '../app/views/posts/index.php';
	}

	public function create()
	{
		require_once '../app/views/posts/create.php';
	}

	public function store()
	{
		$data = ['title' => $_POST['title'], 'body' => $_POST['body']];
		// first parameter is set to null for insert. also propagate session to grab logged in user id
		$result = \Blogie\Models\Post::save(null, $data, $this->session);
		// redirect to the posts index
		header('location: /?controller=posts&action=index');
	}

	public function edit()
	{
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if ( ! isset($_GET['id']))
		{
			return call('pages', 'error');
		}

		// we use the given id to get the right post
		$post = \Blogie\Models\Post::find($_GET['id']);
		if (count($post) !== 0)
		{
			$post = $post[0];
			require_once '../app/views/posts/edit.php';
		}
	}

	public function update()
	{
		$data = ['title' => $_POST['title'], 'body' => $_POST['body']];
		// first parameter is set to null for insert. also propagate session to grab logged in user id
		$result = \Blogie\Models\Post::save($_POST['id'], $data, $this->session);
		// redirect to the posts index
		header('location: /?controller=posts&action=index');
	}

	public function delete()
	{
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if ( ! isset($_GET['id']))
		{
			return call('pages', 'error');
		}

		// we use the given id to get the right post
		$post = \Blogie\Models\Post::find($_GET['id']);
		if (count($post) !== 0)
		{
			$post = $post[0];
			require_once '../app/views/posts/delete.php';
		}
	}

	public function destroy()
	{
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if ( ! isset($_POST['id']))
		{
			return call('pages', 'error');
		}

		$result = \Blogie\Models\Post::destroy($_POST['id']);
		// redirect to the posts index
		header('location: /?controller=posts&action=index');
	}

	public function show()
	{
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if ( ! isset($_GET['id']))
		{
			return call('pages', 'error');
		}

		// we use the given id to get the right post
		$post = Post::find($_GET['id']);
		require_once 'views/posts/show.php';
	}
}
