<?php namespace Blogie\Controllers;

class PagesController {

	private $session;

	public function __construct(\Blogie\Libs\Session $session)
	{
		$this->session = $session;
	}

	public function home()
	{
		// we store all the posts in a variable
		$posts = \Blogie\Models\Post::top();
		require_once '../app/views/pages/home.php';
	}

	public function post()
	{
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if ( ! isset($_GET['id']))
		{
			return call('pages', 'error');
		}

		// we use the given id to get the right post
		$post = \Blogie\Models\Post::find($_GET['id']);
		if (count($post) !== 0)
		{
			$post = $post[0];
			require_once '../app/views/pages/post.php';
		}
	}

	public function error()
	{
		require_once '../app/views/pages/error.php';
	}
}
