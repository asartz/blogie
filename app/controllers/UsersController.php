<?php namespace Blogie\Controllers;

class UsersController {

	private $session;

	public function __construct(\Blogie\Libs\Session $session)
	{
		$this->session = $session;
	}

	public function login()
	{
		require_once '../app/views/users/login.php';
	}

	public function validate()
	{
		// try to validate user. input sanitizing is handled in the model
		$user = \Blogie\Models\User::validate(array('username' => $_POST['username'], 'password' => $_POST['password']));

		// do we have a valid user?
		if ( ! $user === FALSE)
		{
			// set authenticated user to session and redirect to home page
			$this->session->register(120); // Register for 2 hours.
			$this->session->set('user', $user);
			header('location: /?controller=pages&action=home');
		}
		else
		{
			// user is not valid. send him/her back to login page
			header('location: /?controller=users&action=login');
		}
	}

	public function logout()
	{
		$this->session->end();
		header('location: /?controller=pages&action=home');
	}
}
