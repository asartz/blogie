<?php namespace Blogie\Models;

class User {

	private $username;

	private $password;

	public function __construct($username, $password)
	{
		$this->username = $username;
		$this->password = $password;
	}

	public static function validate($credentials)
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		$username = $db->quote($credentials['username']);
		$password = $db->quote($credentials['password']);

		// do not select password under any circumstances. password is hashed using password method.
		$user = $db->select("select id, username from users where username = {$username} and password=password({$password})");

		// do we have a valid user?
		if ( ! empty($user))
		{
			return $user;
		}

		return false;
	}

}