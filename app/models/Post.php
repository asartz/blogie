<?php namespace Blogie\Models;

class Post {

	private $title;
	private $body;

	public static function all()
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		// do not select password under any circumstances
		$posts = $db->select("select id, title, body, updated_at from posts order by id desc");

		return $posts;
	}

	public static function top()
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		// do not select password under any circumstances
		$posts = $db->select("select id, title, body, updated_at from posts order by id desc limit 5");

		return $posts;
	}

	public static function find($id)
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		// get the post by id
		$post = $db->select("select id, title, body, updated_at from posts where id = {$id}");

		return $post;
	}

	public static function save($id = null, $data = [], \Blogie\Libs\Session $session)
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		$title = $db->quote($data['title']);
		$body  = $db->quote($data['body']);

		// grab logged-in user from session
		$user = $session->get('user')[0];

		if (is_null($id))
		{
			$sqlStmt = "insert into posts (title, body, created_at, updated_at, created_by, updated_by) values (left({$title}, 50), left({$body}, 1500), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, {$user['id']}, {$user['id']})";
			$result  = $db->execute($sqlStmt);
		}
		else
		{
			$sqlStmt = "update posts set title=left({$title},50), body=left({$body}, 1500), updated_at=CURRENT_TIMESTAMP, updated_by= {$user['id']} where id={$id}";
			$result  = $db->execute($sqlStmt);
		}

		return $result;
	}

	public static function destroy($id)
	{
		// sanitize user input
		$db = new \Blogie\Libs\Db();

		$sqlStmt = "delete from posts where id={$id}";
		$result  = $db->execute($sqlStmt);

		return $result;
	}
}