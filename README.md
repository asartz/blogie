# README #

A simple php app made for demonstration purposes.

### Keypoints ###

* PSR-4 class autoloading
* Bower package management
* Ultra primitive mvc approach
* Simple configuration through ini file

### Requirements ###

* PHP 5.3+
* Mysql DB

### Who do I talk to? ###

* asartz@versus-software.gr