<?php

require '../vendor/autoload.php';

if (isset($_GET['controller']) && isset($_GET['action']))
{
	$controller = $_GET['controller'];
	$action     = $_GET['action'];
}
else
{
	$controller = 'pages';
	$action     = 'home';
}

// init application wide objects
$session = new Blogie\Libs\Session();

require '../app/views/layout.php';