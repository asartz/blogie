<?php

function call($controller, $action)
{
	global $session;

	switch ($controller)
	{
		case 'pages':
			$controller = new Blogie\Controllers\PagesController($session);
			break;
		case 'posts':
			$controller = new Blogie\Controllers\PostsController($session);
			break;
		case 'users':
			$controller = new Blogie\Controllers\UsersController($session);
			break;
	}

	$controller->{$action}();
}

// we're adding an entry for the new controller and its actions
$controllers = array('pages' => ['home', 'post', 'error'],
	'posts' => ['index', 'create', 'edit', 'delete', 'store', 'update', 'destroy'],
	'users' => ['login', 'validate', 'logout']);

if (array_key_exists($controller, $controllers))
{
	if (in_array($action, $controllers[$controller]))
	{
		call($controller, $action);
	}
	else
	{
		call('pages', 'error');
	}
}
else
{
	call('pages', 'error');
}